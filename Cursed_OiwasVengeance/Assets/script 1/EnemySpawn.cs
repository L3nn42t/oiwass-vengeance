﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public Manager manager;
    public Transform Spawnpoint;
    public GameObject Enemy;
    public GameObject Player;

    public bool EnemyActive = false;

    public void OnTriggerEnter(Collider other)
    {
        if (manager.EnemyActive == false)
        {
            GameObject spawned = Instantiate(Enemy, Spawnpoint);

            manager.EnemyActive = true;

            spawned.GetComponent<EnemyAI>().manager = manager;
            spawned.GetComponent<EnemyAI>().Player = Player;
            Debug.Log("Enemy spawned");
        }
        

        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
