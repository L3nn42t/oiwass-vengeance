﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextInteraction : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject Interactable;
    public GameObject ReadPoint;
    public Transform ReadPosition;

    public bool interacting = false;

    public bool Read = false;
    public CharacterController characterController;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;

        if(interacting == true)
        {
            Interactable.transform.position = ReadPosition.transform.position;
            Interactable.transform.rotation = ReadPoint.transform.rotation;
        }
    }

    void OnMouseOver()
    {
        if (Distance <= 3)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }


        if (Input.GetButtonDown("Action"))
        {
            if (Distance <= 3)
            {
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
                interacting = true;
                //Interactable.transform.position = ReadPosition.transform.position;
                //Interactable.transform.rotation = ReadPoint.transform.rotation;
                Read = true;
                characterController.enabled = false;
            }
        }

        if (Read == true)
        {
            if (Input.GetButton("Dismiss"))
            {
                if (Distance <= 3)
                {
                    interacting = false;
                    Object.Destroy(Interactable);
                    ActionDisplay.SetActive(false);
                    ActionText.SetActive(false);
                    characterController.enabled = true;
                }
            }
        }
    }
    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
