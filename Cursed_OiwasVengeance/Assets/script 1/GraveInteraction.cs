﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GraveInteraction : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    
    public Manager manager;

    [SerializeField]
    private string sceneName;

    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;
    }

    void OnMouseOver()
    {
        if (Distance <= 3)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }
        if (manager.Shovel == true && manager.Skull == true)
        { 
            if (manager.Sword == true)
            {
                if (Input.GetButtonDown("Action"))
                {
                    if (Distance <= 3)
                    {

                        ActionDisplay.SetActive(false);
                        ActionText.SetActive(false);

                        SceneManager.LoadScene(sceneName);

                    }
                }
            }
            
        }

    }
    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
