﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubInteraction : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;

    public GameObject Interactable;
    public GameObject Text;
   

    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;

    }

    void OnMouseOver()
    {
        if (Distance <= 3)
        {
            ActionDisplay.SetActive(true);

        }
        if (Input.GetButtonDown("Action"))
        {
            if (Distance <= 3)
            {
                StartCoroutine(Subtitles());
            }
        }
    }
    IEnumerator Subtitles()
    {
        Text.SetActive(true);
        yield return new WaitForSeconds(5f);
        Text.SetActive(false);
    }
    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
    }
}
