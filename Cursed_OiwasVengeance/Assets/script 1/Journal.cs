﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Journal : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;
    public GameObject Interactable;
    public GameObject InteractionText;
    [SerializeField]
    private bool JournalRead = false;

    [SerializeField]
    private string SceneName;

    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;

        if(JournalRead == true)
        {
            SceneManager.LoadScene(SceneName);
        }
    }

    void OnMouseOver()
    {
        if (Distance <= 2)
        {
            ActionDisplay.SetActive(true);

        }
        if (Input.GetButtonDown("Action"))
        {
            if (Distance <= 3)
            {
                
                StartCoroutine(Time());
                
                
            }
        }
    }

    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
    }

    IEnumerator Time()
    {
        InteractionText.SetActive(true);
        yield return new WaitForSeconds(6f);
        InteractionText.SetActive(false);
        JournalRead = true;
    }
}
