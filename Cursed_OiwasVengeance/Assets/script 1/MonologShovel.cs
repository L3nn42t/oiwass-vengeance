﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonologShovel : MonoBehaviour
{
    public GameObject Shoveltext;
    public Manager manager;

    public void Update()
    {
        if (manager.Shovel == true)
        {
            if (Shoveltext != null)
            {
                StartCoroutine(MonologShovel());
            }
            else
            {
                StopCoroutine(MonologShovel());
                Destroy(this);
            }
        }
        IEnumerator MonologShovel()
        {
            Shoveltext.SetActive(true);
            yield return new WaitForSeconds(5f);
            Shoveltext.SetActive(false);
            Destroy(this);
            //Destroy(Swordtext);
        }
    }
}

