﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullInteraction : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject Skull;
    public Manager manager;



    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;
    }

    void OnMouseOver()
    {
        if (Distance <= 3)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }
        if(manager.Shovel == true)
        {
            if (Input.GetButtonDown("Action"))
            {
                if (Distance <= 3)
                {

                    ActionDisplay.SetActive(false);
                    ActionText.SetActive(false);

                    manager.Skull = true;

                    Destroy(Skull);

                }
            }
        }
       
    }
    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
