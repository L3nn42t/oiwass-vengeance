﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BacktoMainMenu : MonoBehaviour
{
    public string Scenename;

    public void SceneLoader()
    {
        SceneManager.LoadScene(Scenename);
    }

}
