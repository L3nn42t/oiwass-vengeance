﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NoticeBoard : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;
    public GameObject Interactable;
    public GameObject InteractionText;
    public float readtime = 15;

    [SerializeField]
    private string SceneName;

    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;

    }

    void OnMouseOver()
    {
        if (Distance <= 2)
        {
            ActionDisplay.SetActive(true);

        }
        if (Input.GetButtonDown("Action"))
        {
            if (Distance <= 3)
            {
                StartCoroutine(Time());
                
            }
        }
    }

    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
    }

    IEnumerator Time()
    {
        InteractionText.SetActive(true);
        yield return new WaitForSeconds(readtime);
        InteractionText.SetActive(false);
        SceneManager.LoadScene(SceneName);
    }
}


