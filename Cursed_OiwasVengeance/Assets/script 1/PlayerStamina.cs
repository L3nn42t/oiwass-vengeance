﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour
{
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;

    public float Stamina = 100f;
    public float StaminaAmount;
    public float StaminaRecovery;
    public CharacterController characterController;
    public Slider slider;
    public void FixedUpdate()
    {
        if(controller.m_IsWalking == false)
        {
            Stamina -= StaminaAmount;
        }
        if(controller.m_IsWalking == true)
        {
            if(Stamina <= 100f)
            {
                Stamina += StaminaRecovery;
            }
            
        }
        if (Stamina <= 0)
        {
            StartCoroutine(Exhausted());
        }
    }
    private void Update()
    {
        slider.value = Stamina;
    }

    IEnumerator Exhausted()
    {
        controller.m_UseHeadBob = false;
        characterController.enabled = false;
        yield return new WaitForSeconds(2f);
        Stamina = 25;
        controller.m_UseHeadBob = true;
        characterController.enabled = true;
    }
}
