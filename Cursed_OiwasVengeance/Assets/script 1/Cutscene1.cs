﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscene1 : MonoBehaviour

{

    public Animator animator;


    [SerializeField]
    private string Trigger1;
    [SerializeField]
    private string Trigger2;
    [SerializeField]
    private string Trigger3;
    [SerializeField]
    private string Trigger4;
    [SerializeField]
    private string Trigger5;


    // Start is called before the first frame update
    void Start()
    {
        Endscene();
    }


    void Endscene()
    {
        animator.SetTrigger(Trigger1);
        StartCoroutine(Time());
        animator.SetTrigger(Trigger2);
        StartCoroutine(Time());
        animator.SetTrigger(Trigger3);
        StartCoroutine(Time());
        animator.SetTrigger(Trigger4);
        StartCoroutine(Time());
        animator.SetTrigger(Trigger5);

    }
    IEnumerator Time()
    {
        yield return new WaitForSeconds(3f);
    }
}
   
