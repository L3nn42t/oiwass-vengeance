﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public static int currenthealth = 100;
    public int internalhealth;
    public string Scenename;

    private void Start()
    {
        currenthealth = 100;
    }

    void Update()
    {
        internalhealth = currenthealth;
        if (currenthealth <= 0)
        {
            Debug.Log("Player is dead");
            SceneManager.LoadScene(Scenename);
        }
    }
}
