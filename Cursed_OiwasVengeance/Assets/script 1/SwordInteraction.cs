﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordInteraction : MonoBehaviour
{
    public float Distance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject Sword;
    public Manager manager;



    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;
    }

    void OnMouseOver()
    {
        if (Distance <= 3)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }
        if (Input.GetButtonDown("Action"))
        {
            if (Distance <= 3)
            {

                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);

                manager.Sword = true;

                Destroy(Sword);

            }
        }
    }
    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
