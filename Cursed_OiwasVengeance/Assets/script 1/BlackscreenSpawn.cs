﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackscreenSpawn : MonoBehaviour
{
    public GameObject Blackscreen;
    public Manager manager;
    public AudioSource Sound;
    [SerializeField]
    private float BlackTime;

    private void OnTriggerEnter(Collider other)
    {
        if(manager.EnemyActive == false)
        {
            
            StartCoroutine(ScreenDark());
        }
    }
    IEnumerator ScreenDark()
    {
        Sound.Play();
        Blackscreen.SetActive(true);
        yield return new WaitForSeconds(BlackTime);
        Blackscreen.SetActive(false);
    }
}
