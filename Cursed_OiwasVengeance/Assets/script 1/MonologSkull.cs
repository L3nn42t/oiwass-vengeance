﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonologSkull : MonoBehaviour
{
    public GameObject Skulltext;
    public Manager manager;

    public void Update()
    {
        if (manager.Skull == true)
        {
            if (Skulltext != null)
            {
                StartCoroutine(MonologSkull());
            }
            else
            {
                StopCoroutine(MonologSkull());
                Destroy(this);
            }
        }
        IEnumerator MonologSkull()
        {
            Skulltext.SetActive(true);
            yield return new WaitForSeconds(5f);
            Skulltext.SetActive(false);
            Destroy(this);
            //Destroy(Swordtext);
        }
    }
}

