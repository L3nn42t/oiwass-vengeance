﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonologSword : MonoBehaviour
{
    public GameObject Swordtext;
    public Manager manager;

    public void Update()
    {
        if (manager.Sword == true)
        {
            if (Swordtext != null)
            {
                StartCoroutine(MonologSword());
            }
            else
            {
                StopCoroutine(MonologSword());
                Destroy(this);
            }
        }
        IEnumerator MonologSword()
        {
            Swordtext.SetActive(true);
            yield return new WaitForSeconds(5f);
            Swordtext.SetActive(false);
            Destroy(this);
            //Destroy(Swordtext);
        }
    }
}
