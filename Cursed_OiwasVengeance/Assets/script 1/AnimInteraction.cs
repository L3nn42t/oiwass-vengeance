﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimInteraction : MonoBehaviour
{

    public float Distance;
    public GameObject ActionDisplay;
    public AudioSource Creaking;
    public GameObject Interactable;
    public string AnimatorName;

    public Animator animator;

    void Update()
    {
        Distance = PlayerCasting.DistanceFromTarget;

    }

    void OnMouseOver()
    {
        if (Distance <= 3)
        {
            ActionDisplay.SetActive(true);

        }
        if (Input.GetButtonDown("Action"))
        {
            if (Distance <= 3)
            {
                animator.SetTrigger(AnimatorName);
                Creaking.Play();
            }
        }
    }
    
    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
    }
}
