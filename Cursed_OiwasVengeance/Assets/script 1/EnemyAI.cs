﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public GameObject Player;
    public EnemySpawn Spawn;
    public Manager manager;

    public GameObject Enemy;
    public float enemySpeed = 10f;
    private float enemyWalkSpeed;
    public bool attackTrigger = false;
    public bool isAttacking = false;

    public static float DistanceFromPlayer;
    public float ToPlayer;

    public float EnemyDeathDistance = 20f;

    public bool LineOfSight = true; //Wert Placeholder

    public LayerMask playerLayer;




    private void Awake()
    {


    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = (Player.transform.position - transform.position); //Berechnet Richtung von Objekt zu Player
        RaycastHit PHit;

        Debug.DrawRay(transform.position, direction, Color.red); // Erlaubt nachvollziehung zur Korrektur


        if (Physics.Raycast(transform.position, direction, out PHit))
        {

            ToPlayer = PHit.distance;
            DistanceFromPlayer = ToPlayer;

        }

        if (EnemyDeathDistance <= DistanceFromPlayer)
        {

            Debug.Log("Enemy is dead");
            Destroy(Enemy);
            manager.GetComponent<Manager>().EnemyActive = false;
        }





        transform.LookAt(Player.transform);
        if (LineOfSight == true)
        {
            if (attackTrigger == false)
            {
                enemyWalkSpeed = enemySpeed * 0.01f;
                transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, enemyWalkSpeed);
            }
            if (attackTrigger == true && isAttacking == false)
            {
                //enemySpeed = 0.0f;
                StartCoroutine(InflictDamage());
            }
        }



    }
    void OnTriggerEnter()
    {
        attackTrigger = true;
    }
    void OnTriggerExit()
    {
        attackTrigger = false;
    }

    IEnumerator InflictDamage()
    {
        isAttacking = true;
        yield return new WaitForSeconds(1.1f);
        PlayerHealth.currenthealth -= 20;
        yield return new WaitForSeconds(0.5f);
        isAttacking = false;
    }
    //Vielleicht Eunums benutzen
}
